﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EsimeneProjekt
{
    class Program
    {
        static void Main(string[] args)
        {
            byte by = byte.MaxValue;
            by += 7;
            Console.WriteLine(by);

            int i = 10;
            int j = 12;

            double d1 = 1.3;
            double d2 = 3.4;

            char c1 = 'a';
            char c2 = 'b';
            Console.WriteLine(i+j);
            Console.WriteLine(d1+d2);
            Console.WriteLine(c1+c2);
            Console.WriteLine(d1+c2);

            //----------------
            int a = 1;
            int b = 1;
            int c = 3;
            Console.WriteLine(a==b);
            Console.WriteLine(a==c);
            a = c;
            Console.WriteLine(a == b);
            Console.WriteLine(a == c);

            int x1 = 10;
            int x2 = 20;
            int y1 = ++x1;
            Console.WriteLine("x1={0} ja y1={1}", x1,y1);
            int y2 = x2++;
            Console.WriteLine("x2={0} ja y2={1}", x2, y2);

            int arv1 = 18 % 3;
            int arv2 = 19 % 3;
            int arv3 = 20 % 3;
            int arv4 = 21 % 3;
            Console.WriteLine("arv1={0}, arv2={1}, arv3={2}, arv4={3}", arv1, arv2, arv3, arv4);
        }
    }
}
